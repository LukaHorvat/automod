{-# LANGUAGE OverloadedStrings, RecordWildCards #-}
module Main where

import qualified Data.Text as Text
import Pipes
import Data.String.Conv
import Control.Monad.IO.Class
import Control.Applicative
import Control.Monad
import Data.Semigroup
import Network.Discord
import Text.Regex.PCRE.Heavy
import Data.IORef

reply :: Message -> Text.Text -> Effect DiscordM ()
reply Message{messageChannel=chan} cont = fetch' $ CreateMessage chan cont Nothing

main :: IO ()
main = do
    regRef <- newIORef (either error id $ compileM ".*" [])
    runBot (Bot "Mjk0MjI1OTg4NDE2NzAwNDI2.C7SEdw.T0TnJviXJ7v8yvdSKAiQRbajqvY") $ do
        with ReadyEvent $ \(Init v u _ _ _) ->
            liftIO . putStrLn $ "Connected to gateway v" ++ show v ++ " as user " ++ show u

        with MessageCreateEvent $ \msg@Message{..} ->
            if messageChannel == 294236382203740170 then
                if "!automod" `Text.isPrefixOf` messageContent && not (userIsBot messageAuthor) then
                    case compileM (toS $ Text.drop (Text.length "!automod ") messageContent) [] of
                        Left err -> reply msg $ "Bad regex: " <> toS err
                        Right reg -> liftIO $ writeIORef regRef reg
                else unless (userIsBot messageAuthor) $ do
                    reg <- liftIO $ readIORef regRef
                    unless (messageContent =~ reg) $
                        fetch' $ DeleteMessage msg
            else liftIO (print messageChannel)
